Africa50 is an infrastructure investment platform that contributes to Africa's growth by developing and investing 
in bankable projects, catalyzing public sector capital, and mobilizing private sector funding,
with differentiated financial returns and impact. 

Our mandate covers two main areas:

Project Development: we develop a pipeline of investment-ready, "bankable" infrastructure projects through
the provision of early stage funding, project execution support and stakeholder engagement activities, 
from project development to financial cloase.

Project Finance: following a private equity model, we engage with stakeholders and invest in private sector 

infrastructure projects, including Public Private Partnerships near or post financial close.

Our main industry target sectors are Energy/power, transportation, ICT and Gas. Our investor base is currently composed 
of 27 African countries, the African Development Bank and two African Central Banks, with over $870 million in committed capital.

Our mandate also includes mobilising long-term savings from intuitional investors to complement our Sovereign shareholding.
By investing in sustainable infrastructure, Africa50 aims to contribute to the economic and social development of the continent.

For more information, please visit: www.africa50.com

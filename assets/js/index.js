var deadline = new Date("July 9, 2019 08:00:00").getTime(); 
var dateBegin = new Date("April 24, 2019 08:00:00").getTime();
var timeDiff = deadline - dateBegin;

var x = setInterval(function() { 
    var now = new Date().getTime(); 
    var t = deadline - now; 
    var timeFromTimeDiff = dateBegin - now;

    var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
    var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
    var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
    var seconds = Math.floor((t % (1000 * 60)) / 1000);
    document.querySelector("#timer-days").innerHTML = days;
    document.querySelector("#timer-hours").innerHTML = hours;
    document.querySelector("#timer-minutes").innerHTML = minutes;

    if (t < 0) { 
        clearInterval(x); 
        document.querySelector("#timer-days").innerHTML = "0";
        document.querySelector("#timer-hours").innerHTML = "0";
        document.querySelector("#timer-minutes").innerHTML = "0";
    } 
}, 1000);
$("#goTopBtn").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});
setInterval(function () {
  if (window.innerHeight > document.body.clientHeight) {
    document.querySelector(".footer").style.position = "fixed";
    document.querySelector(".footer").style.bottom = "0";
    document.querySelector(".footer").style.left = "0";
    document.querySelector(".footer").style.right = "0";
  } else {
    document.querySelector(".footer").style.position = "relative"
  }
}, 1000);
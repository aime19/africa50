$("#goTopBtn").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
});

document.querySelectorAll('.downFileTrigger').forEach(function (fileSelector) {
    fileSelector.addEventListener('click', function (e) {
        var el = e.target.getAttribute("data-trigger");
        var input = document.getElementById(el);
        input.addEventListener('change', function(e) {
            fileSelector.innerHTML = e.target.files[0].name;
        });
        input.click();
    });
});

document.querySelectorAll(".radios .radio").forEach(function(el) {
    el.addEventListener('click', function(e) {
        var value = e.target.getAttribute("data-value");
        var data_for = e.target.getAttribute("data-for");
        document.getElementById(data_for).value = value;
        document.querySelectorAll(".radios .radio").forEach(function(radio) {
            radio.classList.remove("active");
        });
        e.target.classList.add("active");
    });
});

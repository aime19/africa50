document.querySelectorAll('.my-tab').forEach(function (node) {
    node.addEventListener('click', function(e) {
        let updateEl = e.target;
        for(var i = 0; i < 10; i++) {
            if (updateEl.getAttribute("data-toggle") == null) {
                updateEl = updateEl.parentElement;
            } else {
                break
            }
        }
        document.querySelectorAll('.tab-toggle').forEach(function(tab) {
            if (tab.classList.contains("d-none")) {
                
            } else {
                tab.classList.add("d-none");
            }
        });
        document.querySelectorAll('.my-tab').forEach(function(tab) {
            if (tab.classList.contains("active")) {
                tab.classList.remove("active");
            }
        });
        updateEl.classList.add("active");
        var updateElID = updateEl.getAttribute("data-toggle");
        document.getElementById(updateElID).classList.remove("d-none");
    })
})